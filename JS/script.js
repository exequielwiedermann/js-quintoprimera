document.getElementById('prueba').innerHTML = "Estos alumnos siempre llegan tarde";

document.createAttribute("a", "href");

document.createElement("audio");

document.removeAttribute("href");

document.removeChild("p");

document.replaceChild("a", "p");

document.insertBefore("div", "p");

//Evento acciones

onload();
onchange();
onmousedown();
onmouseup();
onclick();
oncontextmenu();
onkeypress("96");
onkeyup();
onkeydown();
oninput();
onafterprint();
onbeforeprint();

//Evento escuchar
document.addEventListener("click", tengoClick);

function tengoClick() {
    //código del click;
}

//objecto Location
//var url = "https://www.google.com.ar/alumnos/conFaltas/index.html#arriba"

//URL en su totalidad es location.href(url)
//https => location.protocol
// www.gooogle.com.ar => location.hostname
// /alumnos/conFaltas/index.html => location.pathname
// #arriba => location.hash

//objeto window

window.screenY;
window.screenX;

windows.name;

window.scrollY;
window.scrollX;

window.location;

window.status;

window.close();
window.open();
window.print();

//User agent

function detectoSO() {
    var osName = "Desconocido";

    if (navigator.userAgent.indexOf("Windows") != 1) {
        osName = "Linux Fedora";
    }
    if (navigator.userAgent.indexOf("Android") != 1) {
        osName = "Apple";
    } else {
        alert("Tu sistema operativo no es soportado por mi pagina web");
        return false;
    }
}